const Course = require('../models/Course');
const User = require('../models/User');



//addCourse function
/* module.exports.addCourse = (reqBody) => {
    //Created a variable "newCourse" and instantiate a new "Course" object
    //using the mongoose model.
    //This uses the information from the request body to provide 
    //all the neccessary information
    let newCourse = new Course({
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    });
        // Saves the created object to our DB using .save()
    return newCourse.save().then((course, error)=> {
        //course creation failed
        if(error){
            return false
        }
        //Course creation successful
        else{
            return true
        }
    })
};
 */
module.exports.addCourse = (reqBody, userData) => {
    //Created a variable "newCourse" and instantiate a new "Course" object
    //using the mongoose model.
    //This uses the information from the request body to provide 
    //all the neccessary information
return User.findById(userData.userId)
        .then((result)=>{
    if(!userData.isAdmin){
        return 'You are not the Admin'
    }else{
    let newCourse = new Course({
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    });
        // Saves the created object to our DB using .save()


    return newCourse.save().then((course, error)=> {
        //course creation failed
   
        if(error){
            return false
        }
        //Course creation successful
        else{
            return true
        }
    })
}
 })
};

//getAllCourses

module.exports.getAllCourses = (data) => {
    if(data.isAdmin){
        return Course.find({})
        .then(result =>{
            return result
        })
    } else {
        return false
    }
};


// module.exports.getAllCourses = (data) => {
//     if(data.isAdmin){
//         return Course.find({})
//         .then(result =>{
//             return result
//         })
//     } else {
//         return false
//     }
// }


//retrives all active courses 
module.exports.getAllActive = () => {
    return Course.find({isActive : true})
    .then(result =>{
        return result
    })
};

//retrieve a specific course
module.exports.getCourse = (reqParams) =>{
    return Course.findById(reqParams.courseId)
    .then(result =>{
        return result
    })
};

//

module.exports.updateCourse = (reqParams, reqBody, userData) =>{
    if(userData) {
        let updatedCourse = {
            name: reqBody.name,
            description: reqBody.description,
            price: reqBody.price

        }
        //Syntax: findByIdAndUpdate(document ID, updatesToBeApplied)
        return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse)
        .then((updatedCourse, error)=>{
            if(error){
                return false
            } else{
                return true
            }
        })
    } else {
        return 'You are not an Admin'
    }
}

//Archiving Routes

module.exports.archiveCourse = (reqParams, reqBody, userData) =>{
    if(userData) {
        let archivedCourse = {
           isActive: reqBody.isActive

        }
        //Syntax: findByIdAndUpdate(document ID, updatesToBeApplied)
        return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse)
        .then((updatedCourse, error)=>{
            if(error){
                return false
            } else{
                return true
            }
        })
    } else {
        return 'You are not an Admin'
    }
}






