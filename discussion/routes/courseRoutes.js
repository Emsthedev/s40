const express = require('express');
const router = express.Router();
const courseController = require('../controllers/courseController');
const auth = require('../auth');


//Route for creating a course
// router.post('/', (req,res)=>{
//     courseController.addCourse(req.body)
//     .then(resultFromController => 
//         res.send(resultFromController))
// });

router.post('/', auth.verify, (req,res)=>{
    const userData = auth.decode(req.headers.authorization)
    console.log(userData)

    // if(!courseData.isAdmin){
    //     return res.send('You are not the Admin')
    // }
    courseController.addCourse(req.body, {userId: userData.id, isAdmin: userData.isAdmin})
    .then(resultFromController => 
        res.send(resultFromController))
});



// router.post("/", auth.verify, (req, res)=>{
    
//     const courseData = auth.decode(req.headers.authorization)
//     console.log(courseData)

//     courseController.verifyIsAdmin({isAdmin: courseData.isAdmin})
//     .then(resultFromController => 
//         res.send(resultFromController))
// })


// Route Retrieveing All Courses
router.get('/all', auth.verify, (req,res)=>{
    const userData = auth.decode(req.headers.authorization)
    console.log(userData)

    courseController.getAllCourses(userData)
    .then(resultFromController => 
        res.send(resultFromController))

})

//Route for retrieving isActive
router.get('/', (req,res)=>{
    courseController.getAllActive()
    .then(resultFromController => 
        res.send(resultFromController))
})

//route for retrieving specific course
router.get("/:courseId", (req,res)=>{
    console.log(req.params.courseId);
    courseController.getCourse(req.params)
    .then(resultFromController => 
        res.send(resultFromController))
})

//

router.put('/:courseId', auth.verify, (req,res)=>{
    //auth
    const isAdminData = auth.decode(req.headers.authorization).isAdmin
    console.log(isAdminData)

    courseController.updateCourse(req.params, req.body, isAdminData)
    .then(resultFromController => 
        res.send(resultFromController))
});


//Arcvhiving Course
router.put('/archive/:courseId', auth.verify, (req,res)=>{
    //auth
    const isAdminData = auth.decode(req.headers.authorization).isAdmin
    console.log(isAdminData)

    courseController.archiveCourse(req.params, req.body, isAdminData)
    .then(resultFromController => 
        res.send(resultFromController))
});

module.exports = router